package main;

import java.awt.Desktop;
import java.io.*;
import java.util.Properties;
import java.util.Timer;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;

public class s3Services extends documentService{

	AWSCredentials awsCredentials;
	AmazonS3Client s3Client;
	String bucket;
	
	private  Properties properties = new Properties();
	
    public  void uploadFile(String filepath) {
        File folder = new File(filepath);
        
		if(folder.isFile()){
			String parts[]=filepath.split("/");
			String filename=parts[parts.length-1];
            if (!s3Client.doesObjectExist(bucket, "/" + folder)) {
                PutObjectRequest s3Put = new PutObjectRequest(bucket, "/" + filename, folder).withCannedAcl(CannedAccessControlList.PublicRead);
                s3Client.putObject(s3Put);
                System.out.println("Upload complete: " + folder.getName());
            }
            else {
                System.out.println(folder.getName() + " already exists.");
                PutObjectRequest s3Put = new PutObjectRequest(bucket, "/" + filename.split("\\.")[0]+"(1)"+filename.split("\\.")[1], folder).withCannedAcl(CannedAccessControlList.PublicRead);
                s3Client.putObject(s3Put);
                System.out.println("Upload complete: " + filename.split("\\.")[0]+"(1)"+filename.split("\\.")[1]);
            }
        
		}
		else{
	        File[] listOfFiles = folder.listFiles();

	        for (File file : listOfFiles) {
	        	System.out.println(file.getName());
	            if (file.isFile()) {
	                if (!s3Client.doesObjectExist(bucket, "/" + file.getName())) {
	                    PutObjectRequest s3Put = new PutObjectRequest(bucket, "/" + file.getName(), file).withCannedAcl(CannedAccessControlList.PublicRead);
	                    s3Client.putObject(s3Put);
	                    System.out.println("Upload complete: " + file.getName());
	                }
	                else {
	                    System.out.println(file.getName() + " already exists.");
	                    System.out.println(file.getName().split("\\.").length);
	                    PutObjectRequest s3Put = new PutObjectRequest(bucket, "/" + file.getName().split("\\.")[0]+"(1)"+file.getName().split("\\.")[1], file).withCannedAcl(CannedAccessControlList.PublicRead);
	                    s3Client.putObject(s3Put);
	                    System.out.println("Upload complete: " + file.getName().split("\\.")[0]+"(1)"+file.getName().split("\\.")[1]);
	                }
	            }
	        }
		}

    }
    
    public  void openFile(String filepath) throws IOException {
    	int BUFFER_SIZE = 64 * 1024;
    	S3Object s3object = s3Client.getObject(new GetObjectRequest(bucket, "/"+filepath));
    	InputStream stream = s3object.getObjectContent();
    	byte[] content = new byte[BUFFER_SIZE];
    	BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(System.getProperty("user.dir")+"/"+filepath));
    	int totalSize = 0;
    	int bytesRead;
    	while ((bytesRead = stream.read(content)) != -1) {
    	System.out.println(String.format("%d bytes read from stream", bytesRead));
    	outputStream.write(content, 0, bytesRead);
    	totalSize += bytesRead;
    	}
    	System.out.println("Total Size of file in bytes = "+totalSize);
    	outputStream.close();
    	File outputFile=new File(System.getProperty("user.dir")+"/"+filepath);
    	Desktop.getDesktop().open(outputFile);
    	int openTime = 100*1000; //Let's say it would take 100s to be opened
    	try {
			Desktop.getDesktop().wait(openTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	removeFileOrDirectory(filepath);
    	uploadFile(filepath);	
    }
    
    public  void removeFileOrDirectory(String path){
    	if(new File(path).isFile()){
    		s3Client.deleteObject(bucket, "/"+path);
    	}
    	else{
     	   for (S3ObjectSummary file : s3Client.listObjects(bucket, "/"+path).getObjectSummaries()){
    		   s3Client.deleteObject(bucket, file.getKey());
    		    }
    	}
    	System.out.println("Deletion done");
	
    }
    
    
    public  void renameFileorDirectory(String name, String newName){
    	CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucket, 
    			"/"+name, bucket, "/"+newName);
    	s3Client.copyObject(copyObjRequest);
    	s3Client.deleteObject(new DeleteObjectRequest(bucket, "/"+name));
    	System.out.println("Rename Done");
    }
    
    public void getAttribute(String name){
    	ObjectMetadata om=s3Client.getObjectMetadata(bucket, "/"+name);
    	System.out.println("file/directory name: "+name);
    	System.out.println("size: "+om.getContentLength());
    	System.out.println("Last Modified Date: "+om.getLastModified());
    }
    
    @SuppressWarnings("deprecation")
	public  void setConfig(String Property){
    	try {
			properties.load(new FileInputStream(Property));
		} catch (IOException e) {
			e.printStackTrace();
		}
        String awsKey = properties.getProperty("AWSAccessKey");
        String awsSecret = properties.getProperty("AWSAccessSecret");
        bucket=properties.getProperty("Bucket");

        if (awsKey != null && !awsKey.isEmpty() && awsSecret != null && !awsSecret.isEmpty()) {
        	awsCredentials= new BasicAWSCredentials(awsKey, awsSecret);
        }
        else{
        	awsCredentials = null;
        }
        
        s3Client = awsCredentials != null ? new AmazonS3Client(awsCredentials) : new AmazonS3Client();
        
    }
    
}

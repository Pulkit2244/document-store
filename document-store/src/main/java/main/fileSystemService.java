package main;

import java.awt.Desktop;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;


public class fileSystemService extends documentService{
	String loginname,host,password;
	int port;
	FTPClient client;
	private  Properties properties = new Properties();
	String url;
	URLConnection connection;
	String charset = "UTF-8";
	String param = "value";
	String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
    public  void uploadFile(String filepath) throws IOException {
    	
    	if(new File(filepath).isFile()){
        	String parts[]=filepath.split("/");
        	int BUFFER_SIZE = 64 * 1024;
        	String uploadPath=parts[parts.length-1];
        	ftpUrl = String.format(ftpUrl, loginname, password, host, uploadPath);
        	System.out.println("Upload URL: " + ftpUrl);

        	try {
        		

        	    if(new File(filepath).isDirectory()){
        	    	client = new FTPClient();
        	    	client.connect(host, port);
        	    	client.login(loginname, password);
        	    	client.makeDirectory(filepath);
        	    	System.out.println("Directory has been created");
        	    }
        	    else{
            	    URL url = new URL(ftpUrl);
            	    URLConnection conn = url.openConnection();
            	    OutputStream outputStream = conn.getOutputStream();
            	    FileInputStream inputStream = new FileInputStream(filepath);

            	    byte[] buffer = new byte[BUFFER_SIZE];
            	    int bytesRead = -1;
            	    while ((bytesRead = inputStream.read(buffer)) != -1) {
            	        outputStream.write(buffer, 0, bytesRead);
            	    }

            	    inputStream.close();
            	    outputStream.close();

            	    System.out.println("File uploaded");
        	    }
        	} catch (IOException ex) {
        	    ex.printStackTrace();
        	}

    	}
    	else{
    		File folder= new File(filepath);
    		File[] listOfFiles = folder.listFiles();
    		for (File file : listOfFiles) {
    			uploadFile(file.getName());
    		}
    	}

    }
    
    public  void uploadFile(String folder,String filepath) throws IOException {
    	int BUFFER_SIZE = 64 * 1024;
    	ftpUrl = String.format(ftpUrl, loginname, password, host, folder+"/"+filepath);
    	System.out.println("Upload URL: " + ftpUrl);

    	try {
    	    URL url = new URL(ftpUrl);
    	    URLConnection conn = url.openConnection();
    	    OutputStream outputStream = conn.getOutputStream();
    	    FileInputStream inputStream = new FileInputStream(filepath);

    	    byte[] buffer = new byte[BUFFER_SIZE];
    	    int bytesRead = -1;
    	    while ((bytesRead = inputStream.read(buffer)) != -1) {
    	        outputStream.write(buffer, 0, bytesRead);
    	    }

    	    inputStream.close();
    	    outputStream.close();

    	    System.out.println("File uploaded");
	    
    	} catch (IOException ex) {
    	    ex.printStackTrace();
    	}

    }
    
    
    public  void openFile(String filepath) throws IOException {
    	String parts[]=filepath.split("/");
    	String filename=parts[parts.length-1];
    	FileOutputStream dfs=new FileOutputStream(System.getProperty("user.dir")+"/"+filename);
    	client = new FTPClient();
    	client.connect(host, port);
    	client.login(loginname, password);
    	client.retrieveFile(filename, dfs);
    	client.deleteFile(filepath);
    	File outputFile=new File(System.getProperty("user.dir")+"/"+filename);
    	Desktop.getDesktop().open(outputFile);
    	int openTime = 100*1000; //Let's say it would take 100s to be opened
    	try {
			Desktop.getDesktop().wait(openTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	String uploadFolder="";
    	if(parts.length-2<0){
        	for(int i=0;i<parts.length-2;i++){
        		uploadFolder+=parts[i];
        	}
        	uploadFile(uploadFolder,filename);
    	}
    	else{
    		uploadFile(System.getProperty("user.dir")+"/"+filename);
    	}

    	
    	
    }
    
    public  void removeFileOrDirectory(String path) throws IOException{
    	client = new FTPClient();
    	client.connect(host, port);
    	client.login(loginname, password);
    	client.deleteFile(path);
    	client.disconnect();
    }
    
    public  void renameFileorDirectory(String name, String newName) throws IOException{
    	client = new FTPClient();
    	client.connect(host, port);
    	client.login(loginname, password);
    	client.rename(name, newName);
    	client.disconnect();
    }
    
    public void getAttribute(String name) throws IOException{
    	client = new FTPClient();
    	client.connect(host, port);
    	client.login(loginname, password);
    	FTPFile fp=client.mlistFile(name);
    	String time = client.getModificationTime(name);
    	System.out.println("file/directory name: "+name);
    	System.out.println("size: "+fp.getSize());
    	System.out.println("Last Modified Date: "+time);
    }
    
    @SuppressWarnings("deprecation")
	public  void setConfig(String Property) throws MalformedURLException, IOException{
    	try {
			properties.load(new FileInputStream(Property));
		} catch (IOException e) {
			e.printStackTrace();
		}
    	host=properties.getProperty("Host");
    	port=Integer.parseInt(properties.getProperty("Port"));
    	loginname=properties.getProperty("Username");
    	password=properties.getProperty("Password");

        
    }
}

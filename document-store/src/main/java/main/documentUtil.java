package main;

import java.io.*;
import java.net.MalformedURLException;
import java.util.*;


public class documentUtil 
{
	private  Properties properties = new Properties();

	documentService ds;
	public void uploadFiles(String name) throws IOException{
		ds.uploadFile(name);
	}
	public void deleteFiles(String name) throws IOException{
		ds.removeFileOrDirectory(name);
	}
	public void openFiles(String name) throws IOException{
		ds.openFile(name);
	}
	public void renamefile(String name, String newName) throws IOException{
		ds.renameFileorDirectory(name, newName);
	}
	public void fileAttributes(String name) throws IOException{
		ds.getAttribute(name);
	}
	
	
    public  void setConfig(String propertyFilepath) throws MalformedURLException, IOException{
    	
        try {
        	System.out.println(propertyFilepath);
        	properties.load(new FileInputStream(propertyFilepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        String storeType=properties.getProperty("StoreType");
        System.out.println(properties.getProperty("AWSAccessKey"));
        System.out.println(storeType);
        
        if(storeType.equals("S3")){
        	System.out.println("Entered S3");
        	ds= new s3Services();
        	ds.setConfig(propertyFilepath);
        }
        else if(storeType.equals("FileSystem")){
        	ds= new fileSystemService();
        	ds.setConfig(propertyFilepath);
        }
        
//        switch (storeType){
//        case "S3":
//        	System.out.println("Entered");
//        	ds= new s3Services();
//        	ds.setConfig(propertyFilepath);
//        case "FileSystem":
//        	ds= new fileSystemService();
//        	ds.setConfig(propertyFilepath);
//        	
//        case "Database":
//        	// same like filesystem and S3 we can add features for databases
//        	
//        	
//        }
    	
    	
    }
}
